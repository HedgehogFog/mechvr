﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildDestroying : MonoBehaviour {

	private ObjectHealth objectHealth;
	private int health;
	private Animator buildAnimator;

	// Use this for initialization
	void Start () {
		objectHealth = (ObjectHealth) GetComponent("ObjectHealth");
		//health = objectHealth.health;

		buildAnimator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		health = objectHealth.health;
		//Debug.Log ("Health:" + health);
		if (health == 0) {
			buildAnimator.SetTrigger ("LowHealthTrigger");
			health-=1;
		}
			


	}
}
