﻿using UnityEngine;
using System.Collections;

public class GlobalInterfaceController : MonoBehaviour {

	private bool sf_active = true;
	private bool wp_active = true;

	private WayPoints wayPoints;
	private GameObject gun;
	private StayAndFire stayAndFire;
	private GameObject mainCamera;
	private GameObject gunCamera;

	private GameObject button1;
	private GameObject button2;
	private GameObject button3;
	private ButtonScript buttonScript1;
	private ButtonScript buttonScript2;
	private ButtonScript buttonScript3;


	// Use this for initialization
	void Start () {
		wayPoints = (WayPoints) GetComponent("WayPoints");
		gun = GameObject.Find ("Gun");
		stayAndFire = (StayAndFire) gun.GetComponent ("StayAndFire");

		mainCamera = GameObject.Find ("Main Camera");
		gunCamera = GameObject.Find ("Gun Camera");
		gunCamera.SetActive (false);

		button1 = GameObject.Find ("Button1");
		buttonScript1 = (ButtonScript) button1.GetComponent ("ButtonScript");
		button2 = GameObject.Find ("Button2");
		buttonScript2 = (ButtonScript) button2.GetComponent ("ButtonScript");
		button3 = GameObject.Find ("Button3");
		buttonScript3 = (ButtonScript) button3.GetComponent ("ButtonScript");
	}
	
	// Update is called once per frame
	void Update (){
		if (buttonScript1.GetActive()) {
			sf_active = !sf_active;

			//wayPoints.enabled = active;
			mainCamera.SetActive (sf_active);

			stayAndFire.enabled = !sf_active;
			gunCamera.SetActive (!sf_active);
	    }
		if (buttonScript2.GetActive()) {
			wp_active = !wp_active;
			wayPoints.enabled = wp_active;
		}

		//возвращение пушки в начальное положение(поворот)
		if (!sf_active == false) {
			stayAndFire.ReturningToBeginPosition();
			}

         }

	     
    }


					
					