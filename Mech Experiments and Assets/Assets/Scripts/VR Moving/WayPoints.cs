﻿using UnityEngine;
using System.Collections;

public class WayPoints : MonoBehaviour {

	private GameObject[] points; 
	private Vector3 target_Pos;
	private int i = 0;
	[SerializeField]
	private float speed_move = 0.005f;
	[SerializeField]
	private float rotationSpeed = 0.005f;
	private Rigidbody mechRigidbody;

	void Start () {
		//получение массива всех точек
		points = GameObject.FindGameObjectsWithTag("Point");
		//получение каждой точки по имени и перезапись в тот же массив
		//сортировки по имени метод поиска объектов по тегу не предусматривает, и это печально
		for (int i = 0; i <points.Length; i++){
			GameObject wayPointGO = GameObject.Find( "Point" +  (i+1) );
			points [i] = wayPointGO;
		}

		mechRigidbody = GetComponent<Rigidbody> ();
	}
		
	void Update () {
		//позиция точки
		target_Pos = points[i].transform.position;
		//поворот в сторону конечной чели(точки)
		SetRotation(points[i].transform.position);
		//движемся
		mechRigidbody.transform.Translate(Vector3.Normalize(target_Pos - transform.position)*Time.deltaTime*speed_move);
		//вычисляем расстояние до точки
		float distance = Vector3.Distance(target_Pos, transform.position);

		if (distance < 0.5f) {
			if (i < points.Length - 1) {
				i++;
			} else {
				//поправить!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				speed_move = 0;
			}
		}
	}


	void SetRotation(Vector3 lookAt)
	{
		Vector3 lookPos = lookAt - transform.position;
		lookPos.y = 0;
		Quaternion rotation = Quaternion.LookRotation(lookPos);
		mechRigidbody.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
	}
}



