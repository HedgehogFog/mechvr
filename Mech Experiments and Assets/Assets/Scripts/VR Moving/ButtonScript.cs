﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

	[SerializeField]
	private bool active = false;

	void OnMouseDown (){
		Debug.Log ("Clicked!!!");
		active = !active;
	}

	public bool GetActive(){
		return active;
	}
}
