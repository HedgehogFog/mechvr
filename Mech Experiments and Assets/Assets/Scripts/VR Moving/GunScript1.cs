﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript1 : MonoBehaviour {

	public float distance = 100f; //Допустимая дистанция
	public float rateOfSpeed = 0.5f; //Скорость стрельбы

	private float _rateOfSpeed; //Время с последнего выстрела

	public Transform metaHit; //объект при попадании в цель
	public Transform sparksMetal; //Искры при попадании

	private RaycastHit hit; //Объект в который произведенно попадание
	private AudioSource source;

	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;

		source = gameObject.GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape))
			Cursor.lockState = CursorLockMode.None;

		//Проверка на время последнего выстрела
		if (_rateOfSpeed <= rateOfSpeed) 
			_rateOfSpeed += Time.deltaTime;

		if (Input.GetMouseButtonDown (0))
			source.Play ();
		if (Input.GetMouseButtonUp (0))
			source.Stop ();
		
		if (Input.GetMouseButton (0) && _rateOfSpeed > rateOfSpeed) {
			_rateOfSpeed = 0;
			Vector3 DirectionRay = transform.TransformDirection (Vector3.forward);

			if (Physics.Raycast (transform.position, DirectionRay, out hit, distance)) {
				Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);

				if (hit.rigidbody)
					hit.rigidbody.AddForceAtPosition(DirectionRay*100, hit.point);

				//if (hit.collider.material.staticFriction == 0.2f) {
				Transform sparksMetalGo = Instantiate (sparksMetal, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
				//}

				if (hit.collider) {	
					Transform metaHitGo = Instantiate (metaHit, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
					metaHitGo.parent = hit.transform;
					//metaHitGo.GetComponent<Renderer> ().material.color.a = 0.5f;
						metaHitGo.GetComponent<Collider> ().enabled = false;
					Destroy ((metaHitGo as Transform).gameObject, 3);
				}

				//Проверка на существование хит-поинтов у объект
				if (hit.collider.gameObject.GetComponent<ObjectHealth> ())
					hit.collider.gameObject.GetComponent<ObjectHealth> ().activisionHit ();

				//if (hit.collider.tag == "Enemy") {
				//	Destroy (hit.collider.gameObject);
				//}
			}
		} 
	}
}
