﻿using UnityEngine;
using System.Collections;

public class StayAndFire : MonoBehaviour {
	private float XSensevity = 1f;
	private float YSensevity = 1f;
	private float xRotation;
	private float yRotation;

	private float xValue;
	private float yValue;

	private static float gunStartXrotation;
	private static float gunStartYrotation;

	// Use this for initialization
	void Start () {
	    xRotation = transform.rotation.eulerAngles.x;
		yRotation = transform.rotation.eulerAngles.y;

		gunStartXrotation = transform.rotation.eulerAngles.x;
		gunStartYrotation = transform.rotation.eulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {
		xValue = Input.GetAxis("Mouse X")*XSensevity;
		yValue = Input.GetAxis("Mouse Y")*YSensevity*(-1);

		//if(yRotation+xValue<... && xRotation+yValue<...) {}
		yRotation += xValue;
		xRotation += yValue;

		if(xRotation>-90 && xRotation<48) transform.rotation = Quaternion.Euler (xRotation, yRotation, 0);
	}

	//если не равно стартовой позиции, то приближает поворот пушки на 0.5f к начальному повороту
	public void ReturningToBeginPosition(){

		if (xRotation != gunStartXrotation ||
		   yRotation != gunStartYrotation) {

			if (xRotation > gunStartXrotation) {
				xRotation -= 0.5f;
			}

			if (xRotation < gunStartXrotation) {
				xRotation += 0.5f;
			}

			if (yRotation > gunStartYrotation) {
				yRotation -= 0.5f;
			}

			if (yRotation < gunStartYrotation) {
				yRotation += 0.5f;
			}

			transform.rotation = Quaternion.Euler (xRotation, yRotation, 0);
		}
	}
}
