﻿using UnityEngine;
using System.Collections;

public class GlobalController : MonoBehaviour {


	private bool sf_active = true;
	private bool wp_active = true;
	private WayPoints wayPoints;

	private GameObject gun;
	private StayAndFire stayAndFire;

	private GameObject mainCamera;
	private GameObject gunCamera;


	// Use this for initialization
	void Start () {
		wayPoints = (WayPoints) GetComponent("WayPoints");
		gun = GameObject.Find ("Gun");
		stayAndFire = (StayAndFire) gun.GetComponent ("StayAndFire");

		mainCamera = GameObject.Find ("Main Camera");
		gunCamera = GameObject.Find ("Gun Camera");
		gunCamera.SetActive (false);

	}
	
	// Update is called once per frame
	void Update (){
		if (Input.GetKeyDown(KeyCode.F)) {
			sf_active = !sf_active;

			//wayPoints.enabled = active;
			mainCamera.SetActive (sf_active);

			stayAndFire.enabled = !sf_active;
			gunCamera.SetActive (!sf_active);
	    }
		if (Input.GetKeyDown(KeyCode.S)) {
			wp_active = !wp_active;
			wayPoints.enabled = wp_active;
		}

		//возвращение пушки в начальное положение(поворот)
		if (!sf_active == false) {
			stayAndFire.ReturningToBeginPosition();
			}

         }

	     
    }


					
					