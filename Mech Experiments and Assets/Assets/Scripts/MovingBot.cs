﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovingBot : MonoBehaviour {

	private enum StateBot{
		Moving,
		Attacking,
		Escaping,
		Rambo
	}; //Состояния моба

	private enum StateHit{ 
		Hit,
		Miss,
		Doubling
	}; //Состояния выстрела

	private NavMeshAgent agent; //Управление движением
	private Transform target; //Цель движения

	private GameObject player; //Игрок
	private float distance; //Текущая дистанция до игрока
	public float distanceToFire = 3f; //Дистанция необходимая для атаки

	public float rateOfSpeed = 0.5f; //Скорострельность
	private float _rateOfSpeed; //Время с прошлого выстрела

	private StateBot currentState; //Текущее состояние моба

	private GameObject[] shelters; //Массив всех укрытий
	private RaycastHit hit; //Объект в который произведенно попадание

	private int maxHealth; //Максмум здоровья моба
	private int unitHealth; //Здоровье моба

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();
		player = GameObject.FindWithTag ("Player");
		target = player.transform;

		maxHealth = gameObject.GetComponent<ObjectHealth> ().health;

		currentState = StateBot.Moving;
	}

	// Update is called once per frame
	void Update () {
		shelters = GameObject.FindGameObjectsWithTag ("Shelters");

		if (_rateOfSpeed <= rateOfSpeed)
			_rateOfSpeed += Time.deltaTime;

		distance = Vector3.Distance (player.transform.position, transform.position);
		unitHealth = gameObject.GetComponent<ObjectHealth> ().health;
		Debug.DrawLine (transform.position, player.transform.position);

		switch (currentState) {
		case StateBot.Moving:
			agent.SetDestination (target.position);

			if (distance <= distanceToFire) {
				agent.Stop ();
				currentState = StateBot.Attacking;
			} 
			break;

		case StateBot.Attacking:


			if (_rateOfSpeed > rateOfSpeed) {
				_rateOfSpeed = 0;

				StateHit stateShot = getAbleToHit ();
				if (stateShot == StateHit.Hit)
					player.GetComponent<ObjectHealth> ().activisionHit ();
				else if (stateShot == StateHit.Doubling) {
					player.GetComponent<ObjectHealth> ().activisionHit ();
					player.GetComponent<ObjectHealth> ().activisionHit ();
				}
			}

			if (unitHealth <= maxHealth / 2)
				currentState = StateBot.Escaping;
			break;

		case StateBot.Escaping:
			GameObject shelter = findNearestShelter ();

			target = shelter.transform;
			agent.SetDestination (target.position);
			agent.Resume ();

			if (_rateOfSpeed > rateOfSpeed) {
				if (Physics.Raycast (transform.position, player.transform.position, out hit, distanceToFire)) {
					if (hit.collider.tag != "Block") {
						StateHit stateShot = getAbleToHit ();
						if (stateShot == StateHit.Hit)
							player.GetComponent<ObjectHealth> ().activisionHit ();
						else if (stateShot == StateHit.Doubling) {
							player.GetComponent<ObjectHealth> ().activisionHit ();
							player.GetComponent<ObjectHealth> ().activisionHit ();
						}
					}
				}
			}

			if (target.GetComponentInParent<ObjectHealth> ().health <= 1) {
				int luck = Random.Range (-1, 2);
				agent.Stop ();
				target = player.transform;

				if (luck == 0) {
					if (distance <= distanceToFire)
						currentState = StateBot.Attacking;
					else
						currentState = StateBot.Moving;
				} else
					currentState = StateBot.Rambo;
			}

			break;

		case StateBot.Rambo:
			agent.stoppingDistance = 25f;
			agent.SetDestination (target.position);

			agent.Resume ();

			if (_rateOfSpeed > rateOfSpeed) {
				if (Physics.Raycast (transform.position, player.transform.position, out hit, distanceToFire)) {
					if (hit.collider.tag != "Block") {
						StateHit stateShot = getAbleToHit ();
						if (stateShot == StateHit.Hit)
							player.GetComponent<ObjectHealth> ().activisionHit ();
						else if (stateShot == StateHit.Doubling) {
							player.GetComponent<ObjectHealth> ().activisionHit ();
							player.GetComponent<ObjectHealth> ().activisionHit ();
						}
					}
				}
			}

			break;
		}


	}

	private StateHit getAbleToHit(){
		int x = 0;
		for (int i = 0; i < 4; i++)
			x += Random.Range (0, 25);

		if (distance <= distanceToFire / 2)
			x += 30;
		else if (distance <= distanceToFire / 4)
			x += 20;
		else if (distance <= distance)
			x += 10;

		if (currentState == StateBot.Attacking &&
			currentState == StateBot.Rambo)
			x += 20;

		int n = 0, count = 0;

		if (x >= 125)
			return StateHit.Doubling;
		else if (x >= 100)
			return StateHit.Hit;
		else if (x >= 90)
			n = 4;
		else if (x >= 70)
			n = 3;
		else if (x >= 50)
			n = 2;
		else
			n = 1;

		for(int i = 0; i < n; i++)
			count += Random.Range(-1, 2);

		if (count > 0) {
			Debug.Log ("HIT!");
			return StateHit.Hit;
		}
		Debug.LogWarning("MISS!");
		return StateHit.Miss;

	}

	private GameObject findNearestShelter (){
		int numMinElement = 0;
		float distanceMinElement = float.MaxValue;

		for (int i = 0; i < shelters.Length; i++) {
			float shelterDist = Vector3.Distance (shelters[i].transform.position, transform.position);
			//Debug.Log(shelters[i].name, + "\nDistance = " + shelterDist + "\nMinDist = 
			if (shelterDist < distanceMinElement) {
				distanceMinElement = shelterDist;
				numMinElement = i;
			}
		}
		return shelters [numMinElement];
	}
}
