﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ActivisionGroup : MonoBehaviour {

	public GameObject group;

	private NavMeshAgent[] agents;
	private MovingBot[] bots;

	// Use this for initialization
	void Start () {
		agents = group.GetComponentsInChildren<NavMeshAgent> ();
		bots = group.GetComponentsInChildren<MovingBot> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider player) {
		if (player.tag == "Player"){
			for (int i = 0; i < agents.Length; i++) {
				agents [i].enabled = true;
				bots [i].enabled = true;
			}
			Destroy (gameObject);
		}

	}
}
